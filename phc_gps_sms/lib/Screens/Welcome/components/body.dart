import 'package:flutter/material.dart';
import 'package:phc_gps_sms/Screens/Login/login_screen.dart';
import 'package:phc_gps_sms/Screens/Signup/signup_screen.dart';
import 'package:phc_gps_sms/Screens/Welcome/components/background.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:phc_gps_sms/components/rounded_buttons.dart';
import 'package:phc_gps_sms/constant.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Welcome to PHC App",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.09),
            SvgPicture.asset(
              "assets/icons/pregnant.svg",
              height: size.height * 0.35,
            ),
            SizedBox(height: size.height * 0.09),
            RoundedButton(
              text: "LOGIN",
              color: kPrimaryColor,
              textColor: kPrimaryLightColor,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "SIGN UP",
              color: kPrimaryLightColor,
              textColor: kPrimaryColor,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
