import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:phc_gps_sms/Screens/Login/login_screen.dart';
import 'package:phc_gps_sms/Screens/Signup/components/background.dart';
import 'package:phc_gps_sms/components/already_have_an_account_check.dart';
import 'package:phc_gps_sms/components/rounded_buttons.dart';
import 'package:phc_gps_sms/components/rounded_input_fields.dart';
import 'package:phc_gps_sms/components/rounded_password_field.dart';
import 'package:phc_gps_sms/constant.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Sign Up",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            SvgPicture.asset(
              "assets/icons/login.svg",
              height: size.height * 0.35,
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            RoundedInputText(
              hintText: "Your Email",
              icon: Icons.email,
              onChange: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
              hintText: "Your Password",
              icon: Icons.lock,
            ),
            RoundedButton(
              text: "Register",
              color: kPrimaryColor,
              textColor: kPrimaryLightColor,
              press: () {},
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
