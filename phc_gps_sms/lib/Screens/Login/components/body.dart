import 'package:flutter/material.dart';
import 'package:phc_gps_sms/Screens/Login/components/background.dart';
import 'package:phc_gps_sms/Screens/Signup/signup_screen.dart';
import 'package:phc_gps_sms/components/already_have_an_account_check.dart';
import 'package:phc_gps_sms/components/rounded_input_fields.dart';
import 'package:phc_gps_sms/components/rounded_password_field.dart';
import 'package:phc_gps_sms/components/rounded_buttons.dart';
import 'package:phc_gps_sms/constant.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Image.asset(
              "assets/images/login_image.png",
              height: size.height * 0.35,
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            RoundedInputText(
              hintText: "Your Email",
              onChange: (value) {},
              icon: Icons.person,
            ),
            RoundedPasswordField(
              hintText: "Your Password",
              onChanged: (value) {},
              icon: Icons.lock,
            ),
            RoundedButton(
              text: "Login",
              color: kPrimaryColor,
              textColor: kPrimaryLightColor,
              press: () {},
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            AlreadyHaveAnAccountCheck(
              login: true,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
